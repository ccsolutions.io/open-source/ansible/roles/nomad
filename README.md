# Ansible Rolle: Nomad auf Debian

Diese Ansible-Rolle installiert und konfiguriert HashiCorp Nomad auf Debian-Systemen. Sie ermöglicht die dynamische Erstellung von Konfigurationen und kann Nomad auf verschiedenen Umgebungen installieren, konfigurieren und updaten.

## Anforderungen (Requirements)

- Ein Debian-basiertes System (Debian 9/10 oder höher).
- Ansible 2.9 oder höher.

## Abhängigkeiten (Dependencies)

Keine.

## Rolle Variablen (Role Variables)

Die folgenden Variablen können angepasst werden, um die Installation und Konfiguration von Nomad auf Ihrem System zu steuern:

```yaml
# Nomad-Version, die installiert werden soll
nomad_version: "1.2.3"

# Nomad-Konfiguration (Schlüssel-Wert-Paare)
nomad_config:
  bind_addr: "0.0.0.0"
  datacenter: "dc1"
  log_level: "DEBUG"
  server: true
  client: true
  bootstrap_expect: 1
```
